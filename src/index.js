//Our Services Section
const tabList = document.querySelectorAll(".service_item");
const tabImage = document.querySelector(".service_tab_img");
tabList.forEach(elem =>{
    elem.onclick = function () {
        classChecker(tabList);
        classAdder(elem);
        serviceTabLogic(elem);
    }
})
//Our Amazing Work Section
let counter = 0;
const workTabs = document.querySelector(".work_flex");
const workImages = document.querySelectorAll(".work_img");
const collectionsTabList = document.querySelectorAll(".work_item");
const loadMoreButton = document.querySelector(".load_more_btn");
workImages.forEach(elem => elem.classList.add("active"));
workTabFilter();
collectionsTabList.forEach(elem =>{
    elem.onclick = function () {
        classChecker(collectionsTabList);
        classAdder(elem);
        imageSetter(elem);
    }
})
loadMoreButton.addEventListener("click",function (){
    loadMoreButton.classList.add("hidden");
    workImages.forEach(elem => elem.classList.remove("hidden"));
})
//About Us Selector
let activePerson = 3;
const personList = document.querySelectorAll(".person_icon");
let personName = document.querySelector(".name");
const personImage = document.querySelector(".person_img");
const arrowLeft = document.querySelector(".arrow_block_left");
const arrowRight = document.querySelector(".arrow_block_right");
arrowLeft.onclick = function () {
    activePerson -= 1;
    if (activePerson > 4){
        activePerson = 1;
    }else if (activePerson < 1){
        activePerson = 4;
    }
   personChooseLogic();
 };
 arrowRight.onclick = function () {
     activePerson += 1;
     if (activePerson > 4){
         activePerson = 1;
     }else if (activePerson < 1) {
         activePerson = 4;
     }
     personChooseLogic();
 };
 function serviceTabLogic(item){
     switch (item.id){
         case "tab_1":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/web%20design/web-design2.jpg");
             break;
         case "tab_2":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/graphic%20design/graphic-design3.jpg");
             break;
         case "tab_3":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/web%20design/web-design7.jpg");
             break;
         case "tab_4":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/web%20design/web-design1.jpg");
             break;
         case "tab_5":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/web%20design/web-design5.jpg");
             break;
         case "tab_6":
             tabImage.removeAttribute("src");
             tabImage.setAttribute("src","img/web%20design/web-design4.jpg");
             break;
     }
 }
 function workTabFilter(){
    workTabs.addEventListener("click", event =>{
        const elementId = event.target.dataset.id
        switch (elementId){
            case "all":
                workImages.forEach(elem => elem.classList.add("active"));
                break;
            case "graphic_design":
                workImages.forEach(elem => elem.classList.remove("active"));
                document.querySelectorAll(".graphic_design").forEach(elem => elem.classList.add("active"))
                break;
            case "web_design":
                workImages.forEach(elem => elem.classList.remove("active"));
                document.querySelectorAll(".web_design").forEach(elem => elem.classList.add("active"))
                break;
            case "landing_page":
                workImages.forEach(elem => elem.classList.remove("active"));
                document.querySelectorAll(".landing_page").forEach(elem => elem.classList.add("active"))
                break;
            case "wordpress":
                workImages.forEach(elem => elem.classList.remove("active"));
                document.querySelectorAll(".wordpress").forEach(elem => elem.classList.add("active"))
                break;
        }
    })
 }
 function personChooseLogic(){
     switch (activePerson) {
         case 1:
             const personOne = document.querySelector("#person_1");
             classChecker(personList);
             classAdder(personOne);
             personName.textContent = "PERSON 1";
             personImage.removeAttribute("src");
             personImage.setAttribute("src","img/about%20theham/person_1.png");
             break;
         case 2:
             const personTwo = document.querySelector("#person_2");
             classChecker(personList);
             classAdder(personTwo);
             personName.textContent = "PERSON 2";
             personImage.removeAttribute("src");
             personImage.setAttribute("src","img/about%20theham/person_2.png");
             break;
         case 3:
             const personThree = document.querySelector("#person_3");
             classChecker(personList);
             classAdder(personThree);
             personName.textContent = "HASAN ALI";
             personImage.removeAttribute("src");
             personImage.setAttribute("src","img/about%20theham/person_3.png");
             break;
         case 4:
             const personFour = document.querySelector("#person_4");
             classChecker(personList);
             classAdder(personFour);
             personName.textContent = "PERSON 4";
             personImage.removeAttribute("src");
             personImage.setAttribute("src","img/about%20theham/person_4.png");
             break;
         default:
             personName.textContent = "HASAN ALI"
             personImage.removeAttribute("src");
             personImage.setAttribute("src","img/about%20theham/person_3.png");
             break;
     }
 }
function classChecker(list){
    list.forEach(elem => elem.classList.remove("active"));
}
function classAdder(item){
    item.classList.add("active");
}
function imageSetter(item){
    for ( let i=1 ; i <= 7;){
        counter = i;
        item.removeAttribute("src");
        item.setAttribute("src",`img/graphic%20design/web-design${i}.jpg`)
        i += 1;
    }
}